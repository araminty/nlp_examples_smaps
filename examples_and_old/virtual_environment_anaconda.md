A virtual environment let's us make a copy of python with different software installed.  This can be helpful if you want to use scripts with different dependencies.  Suppose that you have been using a script that uses tensorflow 1.15 and now you want to run a script that uses tensorflow 2.0.  Running the scripts in virtual environments means you dont need to change the packages installed to your base python environment.

### Starting a virtual environment from console in Anaconda

Create and start the environment:  
```python -m virtualenv environment_name
. environment_name/bin/activate```

To exit you just need the command `deactivate`.

To use with jupyter notebooks you'll want add the virtual environment to ipython:
```ipython kernel install --user --name=environment_name```

And to remove it again:
```jupyter kernelspec uninstall .venv```

Inside this virtual environment you will be free to add or remove packages without fear of conflict.
